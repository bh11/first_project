package first;

import java.util.Scanner;


// @author $Pálovics István
 
public class First {

    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    int[] t1 = new int[3];
    for ( int i = 0; i < t1.length; i++){
        System.out.println("Kérem a(z) " + (i + 1) + ". számot: ");
        t1[i] = scanner.nextInt();
    }
    
    double av = ((t1[0] + t1[1] + t1[2]) / 3.0);
    System.out.println("A számok átlaga: " + av);
    
    int max = ((t1[0]> t1[1]) && (t1[0] > t1[2]))?0:(t1[1] > t1[2])?1:2;
    System.out.println("A legnagyobb szám a(z) " + t1[max]);
    
    if (((t1[0] + t1[1]) > t1[2]) && ((t1[0] + t1[2]) > t1[1]) && ((t1[1] + t1[2]) > t1[0])){
        System.out.println("Lehet háromszög!");
    } else {
        System.out.println("Nem lehet háromszög...");
    }
   
    
        System.out.println("Kérem az első számot: ");
        int i1 = scanner.nextInt();
        
        System.out.println("Kérem a második számot: ");
        int i2 = scanner.nextInt();
        
        int m;
        do{
            System.out.println("Válassza ki a kívánt műveletet");
            System.out.println("1: összeadás");
            System.out.println("2: kivonás");
            System.out.println("3: szorzás");
            System.out.println("4: osztás");
            System.out.println("5: maradékos osztás");
            m = scanner.nextInt();
        } while ((m > 5) || (m <= 0));
        
        if (i2 == 0){
            switch (m){
            case 1: System.out.println(i1 + " + " + i2 + " = " + (i1 + i2)); break;
            case 2: System.out.println(i1 + " - " + i2 + " = " + (i1 - i2)); break;
            case 3: System.out.println(i1 + " * " + i2 + " = " + (i1 * i2)); break; 
            case 4: System.out.println("Nullával nem osztunk"); break;
            case 5: System.out.println("Nullával nem képzünk maradékot"); break;
            }
        } else {
            switch (m){
            case 1: System.out.println(i1 + " + " + i2 + " = " + (i1 + i2)); break;
            case 2: System.out.println(i1 + " - " + i2 + " = " + (i1 - i2)); break;
            case 3: System.out.println(i1 + " * " + i2 + " = " + (i1 * i2)); break;
            case 4: System.out.println(i1 + " / " + i2 + " = " + (1.0 * i1 / i2)); break;
            case 5: System.out.println(i1 + " % " + i2 + " = " + (i1 % i2)); break;
            
            }
    }   
    }

}
