package hw_03twodimarray;

// @author $Pálovics István

import java.util.Scanner;

public class HW_03twoDimArray {

    static int[] selectOfMin(int[][] a) {
        int[] min = new int[]{0, 0, 100};

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] < a[min[0]][min[1]]) {
                    min[0] = i;
                    min[1] = j;
                    min[2] = a[i][j];
                }
            }
        }

        return min;
    }

    static void generateRandomArray(int[][] array) {
        
        System.out.println("Kérem a legkisebb generálható értéket: ");
        int min = readNumber();
        
        System.out.println("Kérem a legnagyobb generálható érétet: ");
        int max = readNumber() + 1;
        
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int) (Math.random() * (max-min)) + min;
            }
        }
    }

    static int readNumber() {
        Scanner scanner = new Scanner(System.in);
        boolean valid = false;
        int number = 0;

        do {
            if (scanner.hasNextInt()) {
                number = scanner.nextInt();
                valid = true;
            } else {
                scanner.next();
            }

        } while (!valid);
        return number;
    }
    
    public static void main(String[] args) {

        System.out.println("Kérem az oszlopok számát: ");
        int x = readNumber();
        
        System.out.println("Kérem a sorok számát: ");
        int y = readNumber();
        int[][] array = new int[x][y];

        generateRandomArray(array);
        
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println("");
        }

        int[] min = selectOfMin(array);

        System.out.println("A legkissebb szám a " + (min[0] + 1) + ". sorban a " + (min[1] + 1) + ". oszlopnál található, értéke " + min[2]);

    }

}
