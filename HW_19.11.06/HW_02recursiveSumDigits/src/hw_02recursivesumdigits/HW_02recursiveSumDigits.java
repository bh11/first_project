package hw_02recursivesumdigits;

// @author $Pálovics István
public class HW_02recursiveSumDigits {

    static int SUM = 0;

    static void summDigits(int num) {
        if (num == 0) {

            return;
        } else {

            summDigits(num / 10);
        }
        SUM += num % 10;

    }

    public static void main(String[] args) {

        int number = 146815;
        summDigits(number);
        System.out.println(SUM);
    }

}
