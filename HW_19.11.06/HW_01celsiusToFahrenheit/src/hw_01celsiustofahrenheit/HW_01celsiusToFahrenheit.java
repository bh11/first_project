package hw_01celsiustofahrenheit;

import java.util.Scanner;

// @author $Pálovics István
public class HW_01celsiusToFahrenheit {

    public static double celsiusToFahrenheit(double a){
        return a * 1.8 + 32;
    }
    
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean valid = false;
        double celsius = 0;

        do {
            System.out.println("Kérem a hőmérsékletet celsiusban: ");
            if (scanner.hasNextDouble()) {
                celsius = scanner.nextInt();
                valid = true;
            } else {
                scanner.next();
            }

        } while (!valid);
        
        
        System.out.println(celsius + "°C átváltva " + celsiusToFahrenheit(celsius) + "°F");
    }

}
