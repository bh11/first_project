package reflection;

//  @author Istvan Palovics
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;

@MyAnnotation(name = "thisIsMyAnnotation", value = "thisIsTheValue")
class Test {

    private int intVar = 0;
    public String stringVar = null;

    public Test() {
        stringVar = "reflectionTest";
        intVar = 42;
    }

    public Test(String str, int i) {
        stringVar = str;
        intVar = i;
    }

    public int getIntVar() {
        return intVar;
    }

    public void setIntVar(int intVar) {
        this.intVar = intVar;
    }

    public String getStringVar() {
        return stringVar;
    }

    public void setStringVar(String stringVar) {
        this.stringVar = stringVar;
    }

    public void method1() {
        System.out.println("A string változó értéke " + stringVar);
    }

    public void method2(int i) {
        System.out.println("A bemenő paraméter " + i);
    }

    private final int method3() {
        System.out.println("Ez a metódus privát");
        return intVar;
    }
}

class Examples {

    public static void main(String args[]) throws Exception {
        Test obj = new Test();

        Class myClass2 = obj.getClass();
        Class myClass = Class.forName("reflection.Test");
        System.out.println("Az osztályunk neve " + myClass.getName());

        Constructor[] constructors = myClass.getConstructors();
        System.out.println("\nA következő konstruktorokkal hozhatjuk létre az osztályt:");
        for (Constructor c : constructors) {
            
//            System.out.println("A konstruktora " + c);

            Class[] parameters = c.getParameterTypes();
            System.out.print("A konstruktora " + c.getName() + "(");
            for (Class parameter : parameters) {
                System.out.print(parameter.getName() + ", ");
            }
            System.out.println(")");
        }

        Method[] methods = myClass.getMethods();

        System.out.println("\nA következő metódusokkal rendelkezik: ");
        for (Method method : methods) {
            System.out.print(method.getName() + ", ");
        }

        Method[] declaredMethods = myClass.getDeclaredMethods();

        System.out.println("\n\nEzek közül a következő metódusokat nem örökölte: ");
        for (Method method : declaredMethods) {
            System.out.print(method.getName() + ", ");
        }

        System.out.println("\n\nGetterek és Setterek listázása:");
        for (Method method : methods) {
            if (isGetter(method)) {
                System.out.println("getter: " + method);
            }
            if (isSetter(method)) {
                System.out.println("setter: " + method);
            }
        }

        Method methodCall = myClass.getDeclaredMethod("method2", int.class);

        System.out.println("\nMeghívunk egy metódust az invoke segítségével: ");
        methodCall.invoke(obj, 42);

        Field field = myClass.getDeclaredField("stringVar");

        field.set(obj, "kiscica");

        methodCall = myClass.getDeclaredMethod("method1");

        System.out.println("\nMeghívjuk a metódust ami a módosított string értékét írja ki: ");
        methodCall.invoke(obj);

        methodCall = myClass.getDeclaredMethod("method3");
        methodCall.setAccessible(true);

        System.out.println("\nMeghívjuk a privát metódusunkat: ");
        methodCall.invoke(obj);
        System.out.println("\nA metódus visszatérési értéke " + methodCall.getReturnType());
        System.out.println(methodCall.getName() + " metódus láthatóságának értéke " + methodCall.getModifiers());
        System.out.println(methodCall);

        System.out.println("\nReflectionnel létrehozható tömb, majd feltölthető elemekkel");
        int[] intArray = (int[]) Array.newInstance(int.class, 3);
        Array.set(intArray, 0, 3);
        Array.set(intArray, 1, 1);
        Array.set(intArray, 2, 4);
        System.out.println(intArray[0] + "," + intArray[1] + intArray[2]);

        System.out.println("\nAnnotációk elérése reflectionnel");
        Annotation[] annotations = myClass.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof MyAnnotation) {
                MyAnnotation myAnnotation = (MyAnnotation) annotation;
                System.out.println("name: " + myAnnotation.name());
                System.out.println("value: " + myAnnotation.value());
            }
        }

    }

    public static boolean isGetter(Method method) {
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getParameterTypes().length != 0) {
            return false;
        }
        if (void.class.equals(method.getReturnType())) {
            return false;
        }
        return true;
    }

    public static boolean isSetter(Method method) {
        if (!method.getName().startsWith("set")) {
            return false;
        }
        if (method.getParameterTypes().length != 1) {
            return false;
        }
        return true;
    }
}
