package hw_05method;


// @author $Pálovics István


public class HW_05method {

    static int search(String a, char b){
        int occur = 0;
        for (int i = 0; i < a.length(); i++) {
            if (a.charAt(i) == b){
                ++occur;
            }
        }
        return occur;
    }

    
    public static void main(String[] args) {
    
        String input = "az alma piros";
        int num = search(input,'a');
    
        System.out.println("a keresett betű " + num + "szor szerepel a szövegben");
    
    }

}
