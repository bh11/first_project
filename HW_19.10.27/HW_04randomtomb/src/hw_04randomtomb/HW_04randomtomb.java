package hw_04randomtomb;

import java.util.Scanner;


// @author $Pálovics István
 
public class HW_04randomtomb {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean valid = false;
        int[] array = new int[50];
        int sum = 0;
        float avg = 0;
        int num = 0;
        int occur = 0;
        
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 9);
            sum += array[i];
            avg += array[i];
        }
    
        avg /= 1.0 * (array.length);
        
        do {
            System.out.print("Kérem a keresendő számot: ");
            if (sc.hasNextInt()) {

                num = sc.nextInt();
                valid = true;
            } else {
                sc.next();
            }
        } while (!valid);
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] == num) {
                ++occur;
            }
        }
        
        System.out.println(sum);
        System.out.println(avg);
        System.out.println("A szám " + occur + "szor fordul elő");
    }

}
