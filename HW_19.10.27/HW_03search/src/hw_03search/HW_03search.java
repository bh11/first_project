package hw_03search;

import java.util.Scanner;

// @author $Pálovics István
public class HW_03search {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] array = new int[5];
        boolean valid = false;
        int lastIndex = 0;
        int num = 0;
        int match = 0;
        do {
            System.out.print("Kérem a számot: ");
            if (sc.hasNextInt()) {
                array[lastIndex] = sc.nextInt();
                ++lastIndex;
                valid = (lastIndex < array.length);
            } else {
                sc.next();
            }
        } while (valid);

        valid = false;

        do {
            System.out.print("Kérem a keresendő számot: ");
            if (sc.hasNextInt()) {

                num = sc.nextInt();
                valid = true;
            } else {
                sc.next();
            }
        } while (!valid);

        for (int i = 0; i < array.length; i++) {
            if (array[i] == num) {
                ++match;
            }

        }

        if (match == 0) {
            System.out.println("Nincs ilyen szám");
        } else {
            System.out.println(match);
        }
    }

}
