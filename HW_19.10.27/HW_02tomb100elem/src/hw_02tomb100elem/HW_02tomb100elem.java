package hw_02tomb100elem;

import java.util.Scanner;

// @author $Pálovics István
public class HW_02tomb100elem {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] array = new int[100];
        boolean valid = false;
        int lastIndex = -1;
        int maxValue = array[0];
        int maxIndex = 0;
        int minValue = array[0];
        int minIndex = 0;
        
        do {
            System.out.print("Kérem a számot: ");
            if (sc.hasNextInt()) {
                ++lastIndex;
                array[lastIndex] = sc.nextInt();
                valid = (array[lastIndex] == 0);
            } else {
                sc.next();
            }
        } while (!valid);

        for (int i = 0; i < lastIndex; i++) {
            System.out.print(array[i] + " ");
            
            if (maxValue < array[i]) {
                maxValue = array[i];
                maxIndex = i;
            }
            
            if (minValue > array[i]) {
                minValue = array[i];
                minIndex = i;
            }
        }
        System.out.println("");
        for (int i = lastIndex; i >= 0; i--) {
            System.out.print(array[i] + " ");
            
        }
        System.out.println("");
        System.out.println("a legnagyobb értéke és helye " + maxValue + " " + maxIndex);
        System.out.println("a legkissebb értéke és helye " + minValue + " " + minIndex);
        
    }

}
