package hw_01bkk;

import java.util.Scanner;

// @author $Pálovics István
public class HW_01bkk {

    static int hoursRead() {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int hour = 0;

        do {
            System.out.print("Kérem az órát: ");
            if (s.hasNextInt()) {
                hour = s.nextInt();
                valid = (hour >= 0 && hour < 24);
            } else {
                s.next();
            }
        } while (!valid);
        return hour;
    }

    static int minsRead() {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int min = 0;

        do {
            System.out.print("Kérem a percet: ");
            if (s.hasNextInt()) {
                min = s.nextInt();
                valid = (min >= 0 && min < 60);
            } else {
                s.next();
            }
        } while (!valid);
        return min;
    }

    static int penaltyRead() {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int penalty = 0;

        do {
            System.out.print("Kérem a büntetés összegét: ");
            if (s.hasNextInt()) {
                penalty = s.nextInt();
                valid = true;

            } else {
                s.next();
            }
        } while (!valid);
        return penalty;
    }

    static char typeRead() {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        char type = ' ';
        do {
            System.out.print("Kérem a büntetés típusát: ");
            if (s.hasNextLine()) {
                type = s.next().charAt(0);
                valid = (type == 'h' || type == 'c' || type == 'x');

            } else {
                s.next();
            }
        } while (!valid);
        return type;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[][] chart = new int[24][10];
        int hour;
        int min;
        int penalty;
        char type;

        for (int i = 0; i < chart.length; i++) {
            for (int j = 0; j < chart[i].length; j++) {
                chart[i][j] = 0;
            }
        }

        int i = 0;
        do {
            hour = hoursRead();
            min = minsRead();
            type = typeRead();
            penalty = penaltyRead();
            if (type == 'h') {
                chart[hour][i] = penalty;
            } else {
                chart[hour][i] = (int) (penalty * 0.8);
            }
            ++i;
        } while (hour != 0 && min != 0 && penalty != 0 && type != 'x');

        for (int j = 0; j < 24; j++) {
            int sum = 0;
            for (int k = 0; k < chart[j].length; k++) {
                sum += chart[j][k];
            }
            if (sum != 0) {
                System.out.println(j + ":00" + "-" + j + ":59" + " -> " + sum + " Ft");
            }
        }

    }

}
