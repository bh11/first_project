package bh_02writersandbooks;

public class Writer {

    private String name;
    private String birthDate;
    private Book[] books = new Book[10];
    private int numberOfBooks;
    private static final String DEFAULT_B_DATE = "1970.01.01";

    public Writer(String name, String bdate) {
        this.name = name;
        this.birthDate = bdate;
    }
        
    public Writer(String name) {
        this(name, DEFAULT_B_DATE);
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void addBook(int date, String name, String publisher) {
        addBook(new Book(date, name, publisher));
    }

    public void addBook(Book book) {
        if (numberOfBooks < books.length) {
            books[numberOfBooks] = book;
            numberOfBooks++;
        }
    }

    public void printAllBooks() {
        for (int i = 0; i < numberOfBooks; i++) {
            System.out.println(books[i].getName());
        }
    }

}
