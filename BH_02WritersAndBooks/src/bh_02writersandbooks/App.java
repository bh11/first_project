package bh_02writersandbooks;


public class App {


    public static void main(String[] args) {
        
        Writer a = new Writer("Géza Mézga");
        Writer b = new Writer("István Huffnágel", "1969.08.11");
        
        a.addBook(1997, "Alma", "Műegyetem");
        a.addBook(1998, "Barack", "Műegyetem");
        a.addBook(1998, "Citrom","Műegyetem");
        
        b.addBook(1994, "Kutya","Műegyetem");
        b.addBook(1999, "Cica","Műegyetem");
        
        
        System.out.println(a.getName() + " könyvei a következők: ");
        a.printAllBooks();
        System.out.println(b.getName() + " könyvei a következők: ");
        b.printAllBooks();
        
        
    }
    
}
