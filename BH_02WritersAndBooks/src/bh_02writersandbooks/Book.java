package bh_02writersandbooks;


public class Book {
    private int date;
    private String name;
    private String publisher;
    
    public Book(int date, String name, String publisher) {
        this.date = date;
        this.name = name;
        this. publisher = publisher;
    }
    
    public int getDate() {
        return date;
    }
    
    public String getName() {
        return name;
    }
    
    public String getPublisher() {
        return publisher;
    }
    
    public void setPublisher(String name) {
        publisher = name;
    }
}
