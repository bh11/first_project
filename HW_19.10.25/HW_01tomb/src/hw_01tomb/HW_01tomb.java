package hw_01tomb;


// @author $Pálovics István
 
public class HW_01tomb {

    public static void main(String[] args) {
        int[] numbers = new int[50];
        for (int i = 0; i < 50; i++){
            numbers[i] =(int) (Math.random() * 201) - 100;
        }
        int minValue = numbers[0];
        int minIndex = 0;
        int maxValue = numbers[0];
        int maxIndex = 0;
        float average = numbers[0];
        int sum = numbers[0];
        
        for (int i = 1; i < 50; i++) {
            if (numbers[i] < minValue){
                minValue = numbers[i];
                minIndex = i;
            }
            if (numbers [i] > maxValue){
                maxValue = numbers[i];
                maxIndex = i;
            }
            sum += numbers[i];
            average += numbers[i];
        }
        average /= (float) 50;
        
        System.out.println(minValue + ", " + minIndex);
        System.out.println(maxValue + ", " + maxIndex);
        System.out.println(average);
        System.out.println(sum);
    }

}
