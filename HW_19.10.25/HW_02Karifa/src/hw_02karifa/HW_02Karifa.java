package hw_02karifa;

import java.util.Scanner;

// @author $Pálovics István
public class HW_02Karifa {

    public static void main(String[] args) {
        

        for (int i = 0; i < 7; i++) {
            for (int j = 7; j > i; j--) {
                System.out.print("*");
            }
            System.out.println("");
        }
        
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int num = 0;
        do {
            System.out.print("Kérek egy számot: ");

            if (s.hasNextInt()) {
                num = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            }
        } while (!valid);

        int spaces = num / 2;  
        
        for (int i = 0; i < num; i+=2) {
            for (int j = 0; j < spaces; j++) {
                System.out.print(" ");

            }
            for (int j = 0; j <= i; j++) {
                System.out.print("*");

            }
            System.out.println("");

            spaces--;
        }

    }

}
