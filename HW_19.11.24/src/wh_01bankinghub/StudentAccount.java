/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public class StudentAccount extends BankAccount{

    public StudentAccount(String name) {
        super(name);
    }
    
    public void transfer(BankAccount to, int money) {
        boolean success = this.withdrawingMoney(money);
        if (success) {
            to.depositMoney(money);
        }
        
        
    }
}
