/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public class DebitCard extends CreditCard{
    private final Loan loan;

    public DebitCard(Loan loan, BankAccount acc) {
        super(acc);
        this.loan = loan;
    }
    
    public boolean withdrawingMoney(int money) {
        if (ACCOUNT.balance >= money) {
            balance -=money;
            return true;
        } else {
            System.out.println("Nincs fedezet");
            return false;
        }
    }
}
