/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public abstract class Loan {
    protected int amountOfDebt;
    protected static final double INTEREST_RATE = 0.06;
    protected final int INSTALLMENT;
    protected int duration;

    public Loan(int amountOfDebt, int INSTALLMENT) {
        this.amountOfDebt = amountOfDebt;
        this.INSTALLMENT = INSTALLMENT;
    }

    

    public int getAmountOfDebt() {
        return amountOfDebt;
    }

    public void setAmountOfDebt(int amountOfDebt) {
        this.amountOfDebt = amountOfDebt;
    }

    @Override
    public String toString() {
        return "Debit{" + "amountOfDebt=" + amountOfDebt + ", INTEREST_RATE=" + INTEREST_RATE + '}';
    }
    
    
}
