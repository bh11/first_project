/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public class WH_01BankingHub {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        RetailCurrentAccount acc1 = new RetailCurrentAccount("Józsi");
        StudentAccount acc2 = new StudentAccount("Pistike");
        acc1.depositMoney(240500);
        acc2.depositMoney(100500);
        /*System.out.println(acc1);
        System.out.println(acc2);
        acc1.transfer(acc2, 30000);
        System.out.println(acc1);
        System.out.println(acc2);
        acc1.transfer(acc2, 5000);
        System.out.println(acc1);
        System.out.println(acc2);
        acc1.accountManagement();
        System.out.println(acc1);
        acc1.depositMoney(100000);
        acc1.accountManagement();
        System.out.println(acc1);*/
        
        CreditCard card1 = new CreditCard(acc1);
        card1.moneyWithdrawal(10000);
        System.out.println(acc1);
        System.out.println(card1);
        card1.pay(12000);
        System.out.println(acc1);
        System.out.println(card1);
    }
    
}
