/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public class RetailCurrentAccount extends BankAccount {
    private final int ACCOUNT_MANAGEMENT_FEE_BELOW250K = 1500;
    private final int ACCOUNT_MANAGEMENT_FEE_ABOVE250K = 500;
    private final double TRANSFER_FEE_MULTIPLIER = 0.05;
    private final int TRANSFER_FEE_ABOVE10K = 1000;
    
    public RetailCurrentAccount(String name) {
        super(name);
    }
    
    public void transfer(BankAccount to, int money) {
        boolean success;
        if (money > 10000) {
            success = this.withdrawingMoney(money+TRANSFER_FEE_ABOVE10K);
        } else {
            success = this.withdrawingMoney(money+(int)(TRANSFER_FEE_MULTIPLIER*money));          
        }
        if (success) {
            to.depositMoney(money);
        }
    }
    
    public void accountManagement() {
        if (this.getBalance() > 250000) {
            this.withdrawingMoney(ACCOUNT_MANAGEMENT_FEE_ABOVE250K);
        } else {
            this.withdrawingMoney(ACCOUNT_MANAGEMENT_FEE_BELOW250K);
        }
    }
    
}
