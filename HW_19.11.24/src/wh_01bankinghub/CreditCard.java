/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public class CreditCard {
    private final int CARD_NUMBER;
    private final int CARD_REQUEST_FEE = 5000;
    protected final BankAccount ACCOUNT;
    private final double MONEY_WITHDRAWAL_FEE_MULTIPLIER = 0.1;
    private final int MONEY_WITHDRAWAL_FEE_ABOVE10K = 1000;
    
    public CreditCard(BankAccount acc) {
        CARD_NUMBER = (int)(Math.random()*1000000);
        ACCOUNT = acc;
        ACCOUNT.withdrawingMoney(CARD_REQUEST_FEE);
    }

    public int getCARD_NUMBER() {
        return CARD_NUMBER;
    }

    public int getCARD_REQUEST_FEE() {
        return CARD_REQUEST_FEE;
    }

    public BankAccount getACCOUNT() {
        return ACCOUNT;
    }

    @Override
    public String toString() {
        return "CreditCard's owner is " + ACCOUNT.getName() + ", the card number is " + CARD_NUMBER + " and the balance is " + ACCOUNT.getBalance();
    }
    
    public void moneyWithdrawal(int money) {
        if (money > 10000) {
            ACCOUNT.withdrawingMoney(money+MONEY_WITHDRAWAL_FEE_ABOVE10K);
        } else {
            ACCOUNT.withdrawingMoney(money+(int)(MONEY_WITHDRAWAL_FEE_MULTIPLIER*money));          
        }
    }
    
    public void pay(int money) {
        ACCOUNT.withdrawingMoney(money);
    }
    
    
}
