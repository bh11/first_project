/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public abstract class BankAccount {
    private final String name;
    private final int ACCOUNT_NUMBER;
    private int balance;
    private final int ACCOUNT_OPENING_FEE = 500;
    
    public BankAccount(String name) {
        this.name = name;
        ACCOUNT_NUMBER = (int)(Math.random()*1000000);
        balance -= ACCOUNT_OPENING_FEE;
    }

    public int getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public int getACCOUNT_NUMBER() {
        return ACCOUNT_NUMBER;
    }

    public int getACCOUNT_OPENING_FEE() {
        return ACCOUNT_OPENING_FEE;
    }

    @Override
    public String toString() {
        return "BankAccount's owner is " + name + ", the account number is " + ACCOUNT_NUMBER + ", the balance is " + balance;
    }
    
    public void depositMoney(int money) {
        balance += money;
    }
    
    public boolean withdrawingMoney(int money) {
        if (balance >= money) {
            balance -=money;
            return true;
        } else {
            System.out.println("Nincs fedezet");
            return false;
        }
    }
    
}
