/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wh_01bankinghub;

/**
 *
 * @author Szottyos
 */
public class HousingLoan extends Loan {
    private final BankAccount ACCOUNT;

    public HousingLoan(BankAccount ACCOUNT, int amountOfDebt) {
        super(amountOfDebt, (int) ((amountOfDebt*INTEREST_RATE+amountOfDebt)/3));
        //int installment = (int) ((amountOfDebt*INTEREST_RATE+amountOfDebt)/3);
        this.ACCOUNT = ACCOUNT;
        this.duration = 3;
    }
    
    

    public int getINSTALLMENT() {
        return INSTALLMENT;
    }

    public int getDuration() {
        return duration;
    }

    public int getAmountOfDebt() {
        return amountOfDebt;
    }
    
    public void monthlyPayOff() {
        duration--;
        ACCOUNT.withdrawingMoney(INSTALLMENT);
    }
    
    
    
}
