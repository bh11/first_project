package hw_02lambdas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//  @author Istvan Palovics
public class HW_02lambdas {

    public static void print(List<String> list) {
        for (String string : list) {
            System.out.print(string + ", ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>(List.of("ananas", "avocado", "apple", "peach", "plum", "grapefruit", "banana"));

        print(stringList);

        Collections.sort(stringList, (s1, s2)
                -> s1.length() - s2.length());

        print(stringList);

        Collections.sort(stringList, (s1, s2)
                -> s2.length() - s1.length());

        print(stringList);

        Collections.sort(stringList, (s1, s2)
                -> s1.charAt(0) - s2.charAt(0));

        print(stringList);

        Collections.sort(stringList, (String s1,String s2) -> {
            if (s1.contains("e") && s2.contains("e")) {
                return 0;
            } else {
                if (s1.contains("e")) {
                    return -1;
                } else return 1;
            }
        });
           
        print(stringList);

    }

}
