package hw_01enums;

public class HW_01Enums {

    public static void main(String[] args) {
        Weekday day = Weekday.MONDAY;
        
        if (day.isWeekDay()) {
            System.out.println("Hétköznap");
        } else {
            System.out.println("Hétvége");
        }
        
        day = Weekday.SATURDAY;
        
        if (day.isWeekDay()) {
            System.out.println("Hétköznap");
        } else {
            System.out.println("Hétvége");
        }
        
        for (Weekday d : Weekday.values()) {
            System.out.println(d.compareTo(day));
        }
    }

}
