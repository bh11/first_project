package hw_01enums;

import java.util.ArrayList;
import java.util.List;


//  @author Istvan Palovics

public enum Weekday {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
    
    private final List<String> weekend = new ArrayList<>(List.of("SATURDAY","SUNDAY"));
    
    public boolean isHoliday() {
        return weekend.contains(toString());
    }
    
     public boolean isWeekDay() {
        return !(isHoliday());
    }
    
}
