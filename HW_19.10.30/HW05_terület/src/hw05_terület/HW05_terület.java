package hw05_terület;


// @author $Pálovics István
 
public class HW05_terület {

    public static int terület(int a, int b) {
        return (a * b);
    }
    
    
    public static void main(String[] args) {
    
        int a = 8;
        int b = 10;
        
        System.out.println(terület(a, b));
    
    }

}
