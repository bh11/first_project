package hw_02abs;

import java.util.Scanner;


// @author $Pálovics István
 
public class HW_02abs {

    
    public static int bigestAbs(int a, int b){
        if (Math.abs(a) > Math.abs(b)) {
            return a;
        } else return b;
    }
    
    public static void main(String[] args) {
        
    Scanner s = new Scanner(System.in);
        boolean valid = false;
        int a = 0;
        int b = 0;

        do {
            System.out.print("Kérem a számot: ");
            if (s.hasNextInt()) {
                a = s.nextInt();
                valid = true;
            } else {
                s.next();
            }
        } while (!valid);
        
        valid = false;
        do {
            System.out.print("Kérem a számot: ");
            if (s.hasNextInt()) {
                b = s.nextInt();
                valid = true;
            } else {
                s.next();
            }
        } while (!valid);
    
        System.out.println(bigestAbs(a, b));
    }

}
