package hw_01prim;

import java.util.Scanner;


// @author $Pálovics István
 
public class HW_01prim {
    
    public static boolean isPrim (int i){
        for (int j = 2; j < Math.sqrt(i); j++) {
            if (i % j == 0){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
    
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int number = 0;

        do {
            System.out.print("Kérem a számot: ");
            if (s.hasNextInt()) {
                number = s.nextInt();
                valid = true;
            } else {
                s.next();
            }
        } while (!valid);
        
        System.out.println(isPrim(number));
    
    }
}