package hw03_array;

// @author $Pálovics István
public class HW03_array {

    public static void printArrayBackwards(int[] array) {
        for (int i = (array.length - 1); i >= 0; i -= 2) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array1 = {2, 4, 7, 12, 15, 18};
        int[] array2 = {3, 7, 10, 13, 17};

        printArrayBackwards(array1);
        printArrayBackwards(array2);
    }

}
