package hw06_divisibility;


// @author $Pálovics István
 
public class HW06_divisibility {

    public static int div(int[] array) {
        int a = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 5 != 0 && array[i] % 3 == 0){
                a++;
            }
        }
        return a;
    }
    
    
    public static void main(String[] args) {
    
        int[] array = { 3, 5, 10, 15, 18, 21};
        
        System.out.println(div(array));
    
    
    }

}
