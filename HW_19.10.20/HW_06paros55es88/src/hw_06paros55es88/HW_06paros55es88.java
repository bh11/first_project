package hw_06paros55es88;

// @author $Pálovics István
public class HW_06paros55es88 {

    public static void main(String[] args) {
        for (int i = 56; i <= 88; i += 2) {
            System.out.print(i + ", ");
        }
        System.out.println("");

        int num1 = 55;
        int num2 = 88;
        
        while (num1 <= num2 ){
            if (num1 % 2 == 0){
                System.out.print(num1 + ", ");
            }
            num1++;
        }
        
        
    }

}
