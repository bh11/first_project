package hw03_negyzet;

import java.util.Scanner;


// @author $Pálovics István
 
public class HW03_negyzet {

    public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    int num = 0;
    boolean valid = false;
        do {
            System.out.println("Kérek egy pozitív számot: ");
            if (s.hasNextInt()) {
                num = s.nextInt();

                if (num > 0) {
                    valid = true;
                }
            } else {
                s.nextLine();
            }
        } while (!valid);
        
        for (int i = 0; i <= num; i++){
            System.out.print(i * i + ", ");
        }
    
    
    }

}
