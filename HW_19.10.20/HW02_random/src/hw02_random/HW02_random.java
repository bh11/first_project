package hw02_random;

// @author $Pálovics István
public class HW02_random {

    public static void main(String[] args) {
        int num = (int) (Math.random() * 9000 + 1000);
        System.out.println(num);
        if (num == 10000) {
            System.out.println(num / 10000 + " " + num % 10);
        } else {
            System.out.println(num / 1000 + " " + num % 10);

        }

    }

}
