package hw08_siknegyed;

import java.util.Scanner;

// @author $Pálovics István
public class HW08_siknegyed {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int X = 0;
        int Y = 0;

        do {
            System.out.println("Kérem az első számot: ");
            if (s.hasNextInt()) {
                X = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            }
        } while (!valid);
        do {
            System.out.println("Kérem a második számot: ");
            if (s.hasNextInt()) {
                Y = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            }
        } while (!valid);

        if (X >= 0) {
            if (Y >= 0) {
                System.out.println("Első síknegyed");
            } else {
                System.out.println("Negyedik síknegyed");
            }
        } else {
            if (Y >= 0) {
                System.out.println("Második síknegyed");
            } else {
                System.out.println("Harmadik síknegyed");
            }
        }

    }

}
