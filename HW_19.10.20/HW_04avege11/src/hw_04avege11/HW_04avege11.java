package hw_04avege11;

import java.util.Scanner;


// @author $Pálovics István
 
public class HW_04avege11 {

    public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    int num1 = 0;
    int num2 = 0;
    boolean valid = false;
        do {
            System.out.println("Kérem az első számot: ");
            if (s.hasNextInt()) {
                num1 = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            }
        } while (!valid);
        do {
            System.out.println("Kérem a második számot: ");
            if (s.hasNextInt()) {
                num2 = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            }
        } while (!valid);
        
        if (num1 < num2){
            for (int i = num1; i < num2; i++){
                if (i % 100 == 11){
                    System.out.print(i + ", ");
                }
            }
        } else
            for (int i = num2; i < num1; i++){
                if (i % 100 == 11){
                    System.out.print(i + ", ");
                }
            }
    
    
    }

}
