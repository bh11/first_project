package hw05_kisebb10;

import java.util.Scanner;

// @author $Pálovics István
public class HW05_kisebb10 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int num = 0;
        int sum = 0;

        do {
            do {
                System.out.println("Kérem a számot: ");
                if (s.hasNextInt()) {
                    num = s.nextInt();
                    valid = true;
                } else {
                    s.nextLine();
                }
            } while (!valid);
            valid = false;
            sum += num;
        } while (num <= 10);

        System.out.println(sum - num);

    }

}
