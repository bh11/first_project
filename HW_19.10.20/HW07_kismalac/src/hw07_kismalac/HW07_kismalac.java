package hw07_kismalac;

import java.util.Scanner;


// @author $Pálovics István
 
public class HW07_kismalac {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int num = 0;
        
        
        do{
            System.out.println("Kérem a számot: ");
            if(s.hasNextInt()){
                num = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            } 
        } while (!valid);
    
        if (num % 10 == 7){
            System.out.println("kismalac");
        } else {
            System.out.println("nem kismalac");
        }
        
        switch (num % 10){
            case 7: System.out.println("kismalac"); break;
            default: System.out.println("nem kismalac");
        }   
    }

}
