package hw09_lko;

import java.util.Scanner;

// @author $Pálovics István
public class HW09_LKO {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int num1 = 0;
        int num2 = 0;

        do {
            System.out.println("Kérem az első számot 10 és 500 között: ");
            if (s.hasNextInt()) {
                num1 = s.nextInt();
                if (num1 < 500 && num1 > 10) {
                    valid = true;
                }
            } else {
                s.nextLine();
            }
        } while (!valid);
        valid = false;
        do {
            System.out.println("Kérem a második számot 10 és 500 között: ");
            if (s.hasNextInt()) {
                num2 = s.nextInt();
                if (num2 < 500 && num2 > 10) {
                    valid = true;
                }
            } else {
                s.nextLine();
            }
        } while (!valid);

        int maradék;
        int X;
        if (num1 > num2) {
            maradék = (num1 % num2);
            while (maradék != 0) {

                num1 = num2;
                num2 = maradék;
                maradék = (num1 % num2);
            }
            System.out.println("A legnagyobb közös osztó: " + num2);

        } else {
            maradék = (num2 % num1);
            while (maradék != 0) {

                num2 = num1;
                num1 = maradék;
                maradék = (num2 % num1);
            }
            System.out.println("A legnagyobb közös osztó: " + num1);

        }

    }

}
