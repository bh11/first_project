package hw01_parososzto;

// @author $Pálovics István

import java.util.Scanner;

public class HW01_parosOszto {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        boolean valid = false;
        int num = 0;
        do {
            System.out.println("Kérem a számot: ");
            if (s.hasNextInt()) {
                num = s.nextInt();
                valid = true;
            } else {
                s.nextLine();
            }
        } while (!valid);
        for (int i = 2; i <= num; i+=2){
            if ((num % i) == 0){
                System.out.print(i + ", ");
            }
        }
    }
}
