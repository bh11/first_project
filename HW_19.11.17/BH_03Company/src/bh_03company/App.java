package bh_03company;


public class App {


    public static void main(String[] args) {
        Employee employee1 = new Employee("Aranka Tank", "001");
        
        Accountancy acc = new Accountancy("H001", "Hungary");
        Bill bill = new Bill("B001", 500);
        acc.putBillInAccountancy(bill);
        bill = new Bill("B002", 200 );
        acc.putBillInAccountancy(bill);
        bill = new Bill("B003");
        acc.putBillInAccountancy(bill);
        employee1.addAccountancy(acc);
        
        acc = new Accountancy("H002", "Hungary");
        bill = new Bill("B001", 700);
        acc.putBillInAccountancy(bill);
        bill = new Bill("B002");
        acc.putBillInAccountancy(bill);
        bill = new Bill("B003");
        acc.putBillInAccountancy(bill);
        employee1.addAccountancy(acc);
        
        
        employee1.printAccountancy("Hungary");
        employee1.increaseBillsOfAccountancy("Hungary");
        employee1.printAccountancy("Hungary");

    }
    
}
