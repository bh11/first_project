package bh_03company;


public class Bill {
    private String id;
    private int debit;
    
    public Bill(String id){
        this.id = id;
        debit = 1000;
    }
    
    public Bill (String id, int deb) {
        this.id = id;
        debit = deb;
    }
    
    public int getDebit() {
        return debit;
    }
    
    public String getId() {
        return id;
    }
    
    public void setDebit(int debit) {
        this.debit = debit;
    }
}
