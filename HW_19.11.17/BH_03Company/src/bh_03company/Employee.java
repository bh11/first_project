package bh_03company;

public class Employee {

    private String name;
    private String id;
    private Accountancy a;
    private Accountancy acc[];
    private int numberOfAccountancies;

    public Employee(String name, String id) {
        acc = new Accountancy[2];
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addAccountancy(String id, String country) {
        //acc[numberOfAccountancies] = new Accountancy(String id, String country);
    }

    public void addAccountancy(Accountancy acc) {
        this.acc[numberOfAccountancies] = acc;
        numberOfAccountancies++;
    }

    public void addBill(String id) {

    }

    public void increaseBillsOfAccountancy(String country) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getCountry().equals(country)) {
                acc[i].increaseBills();
            }
        }
    }

    public void decreaseBillsOfAccountancy(String country) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getCountry().equals(country)) {
                acc[i].decreaseBills();
            }
        }
    }

    public void printAccountancy(String country) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getCountry().equals(country)) {
                System.out.println(acc[i].getId() + " " + acc[i].getCountry() + " : ");
                acc[i].printBills();
                System.out.println();
            }
        }
    }
}
