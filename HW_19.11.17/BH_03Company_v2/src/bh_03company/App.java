package bh_03company;


public class App {


    public static void main(String[] args) {
        Employee employee1 = new Employee("Aranka Tank", "001");
        
        employee1.addAccountancy("H001", "Hungary");
        employee1.addAccountancy("H002", "Hungary");
        
        employee1.addBill("H001", "B001");
        employee1.addBill("H001", "B002", 500);
        employee1.addBill("H002", "B001");
        employee1.addBill("H002", "B002", 1400);
        
        employee1.printAllAccountancy("Hungary");
        employee1.increaseBillsOfAccountancy("Hungary");
        employee1.printAccountancy("H001");
        employee1.printAccountancy("H002");

    }
    
}
