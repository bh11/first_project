package bh_03company;

public class Employee {

    private String name;
    private String id;
    private Accountancy a;
    private Accountancy acc[];
    private int numberOfAccountancies;

    public Employee(String name, String id) {
        acc = new Accountancy[2];
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addAccountancy(String id, String country) {
        acc[numberOfAccountancies] = new Accountancy(id, country);
        numberOfAccountancies++;
    }

    public void addBill(String accId, String id) {
        Bill bill = new Bill(id);
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getId().equals(accId)) {
                acc[i].putBillInAccountancy(bill);
            }
        }
    }
    
    public void addBill(String accId, String id, int deb) {
        Bill bill = new Bill(id, deb);
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getId().equals(accId)) {
                acc[i].putBillInAccountancy(bill);
            }
        }
    }

    public void increaseBillsOfAccountancy(String country) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getCountry().equals(country)) {
                acc[i].increaseBills();
            }
        }
    }

    public void decreaseBillsOfAccountancy(String country) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getCountry().equals(country)) {
                acc[i].decreaseBills();
            }
        }
    }

    public void printAllAccountancy(String country) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getCountry().equals(country)) {
                System.out.println(acc[i].getId() + " " + acc[i].getCountry() + " : ");
                acc[i].printBills();
                System.out.println();
            }
        }
    }
    
    public void printAccountancy(String id) {
        for (int i = 0; i < numberOfAccountancies; i++) {
            if (acc[i].getId().equals(id)) {
                System.out.println(acc[i].getId() + " " + acc[i].getCountry() + " : ");
                acc[i].printBills();
                System.out.println();
            }
        }
    }
}
