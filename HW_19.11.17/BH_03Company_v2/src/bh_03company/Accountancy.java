package bh_03company;


public class Accountancy {
    private String id;
    private String country;
    private Bill bills[];
    
    private int numberOfBills;
    
    public Accountancy(String id, String country) {
        bills = new Bill[10];
        this.id = id;
        this.country = country;
    }
    
    public String getId(){
        return id;
    }
    
    public String getCountry(){
        return country;
    }

    public void putBillInAccountancy(Bill bill){
        bills[numberOfBills] = bill;
        numberOfBills++;
    }

    public void increaseBills() {
        for (int i = 0; i < numberOfBills; i++) {
            bills[i].setDebit((int)(bills[i].getDebit() * 1.1));
        }
    }
    
    public void decreaseBills() {
        for (int i = 0; i < numberOfBills; i++) {
            bills[i].setDebit((int)(bills[i].getDebit() * 0.9));
        }
    }
    
    public void printBills() {
        for (int i = 0; i < numberOfBills; i++) {
            System.out.println("     " + bills[i].getId() + " " + bills[i].getDebit());
        }
    }
    
}

