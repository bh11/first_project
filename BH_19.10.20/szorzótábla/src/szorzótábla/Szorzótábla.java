package szorzótábla;


// @author $Pálovics István
 
public class Szorzótábla {

    public static void main(String[] args) {
    
        int i = 1;
        while (i <= 10){
            System.out.println(i + "*" + 10 + " = " + (i * 10));
            i++;
        }
    
    
    }

}
