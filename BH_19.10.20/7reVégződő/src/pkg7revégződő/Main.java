package pkg7revégződő;

import java.util.Scanner;


// @author $Pálovics István
 
public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int a = 0;
        int b = 0;
        boolean find = false;
        
        do {
            System.out.println("Adja meg az első számot");
            if (s.hasNextInt()) {
                a = s.nextInt();
                find = true;
            } else {
                s.nextLine();
            }
        } while(!find);
        
        find = false;
        
        do {
            System.out.println("Adja meg a második számot");
            if (s.hasNextInt()) {
                b = s.nextInt();
                find = true;
            } else {
                s.nextLine();
            }
        } while(!find);
        
       int min = a<b?a:b;
       int max = a>b?a:b;
        
        while (min <= max){
            if (min % 10 == 7){
                System.out.print(min + ", ");
            }
            min++;
        }
    
    }

}
