package third;

import java.util.Scanner;

// @author $Pálovics István
public class Third {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        int num = 0;
        /*        System.out.println("Kérek egy számot: ");

                if (s.hasNext()){
            num = s.nextInt();
        }
        switch (num) {
            case 1: System.out.println("I"); break;
            case 2: System.out.println("II"); break;
            case 3: System.out.println("III"); break;
            case 4: System.out.println("IV"); break;
            case 5: System.out.println("V"); break;
            
            default: System.out.println("Ez nem volt a feladatban");
        }
         */

        Scanner scanner = new Scanner(System.in);

        boolean asking = true;

        int number = -1;

        do {

            System.out.println("Adjon meg egy pozitív számot egy és 5 között:");

            if (scanner.hasNextInt()) {

                number = scanner.nextInt();

                if (number >= 1 && number <= 5) {

                    asking = false;

                }

            } else {

                scanner.next();

            }

        } while (asking);

        System.out.println(number);

    }
}
