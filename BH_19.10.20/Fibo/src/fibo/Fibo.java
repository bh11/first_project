package fibo;

import java.util.Scanner;

// @author $Pálovics István
public class Fibo {

    public static void main(String[] args) {
        int a = 1;
        int b = 1;
        int c = 2;
        boolean valid = false;
        int i = 0;
        Scanner s = new Scanner(System.in);

        do {
            System.out.println("Hányadik számjegyre vagy kíváncsi? ");
            if (s.hasNextInt()) {
                i = s.nextInt();

                if (i > 0) {
                    valid = true;
                }
            } else {
                s.nextLine();
            }
        } while (!valid);

        if (i <= 2) {
            System.out.println(1);
        } else {
            if (i == 3) {
                System.out.println(2);
            } else {
                for (int j = 3; j < i; j++) {
                    a = b;
                    b = c;
                    c = a + b;
                }
                System.out.println(c);
            }

        }
    }

}
