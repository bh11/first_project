package first;

import java.util.Scanner;


// @author $Pálovics István
 
public class Fourth {

    public static void main(String[] args) {
    
/*        Scanner s = new Scanner (System.in); //standard input
        int num = s.nextInt();
        
        switch(num){
            case 5: System.out.println("Megfelelt"); break;
            case 4: System.out.println("Megfelelt"); break;
            
            default: System.out.println("Nem felelt meg");
        }
*/   
        switch(num){
            case 5:
            case 4: System.out.println(" Megfelelt"); break;
            case 3:
            case 2:
            case 1: System.out.println("Nem felelt meg"); break;
        }
    
    }

}
