package first;

import java.util.Scanner;


// @author $Pálovics István
 
public class First {

    public static void main(String[] args) {
    
       /* long a = 100000000000000000L; //L nélkül intbe próbálja tenni, és a = int
        byte b = (byte) 128; //csak 127ig tud tárolni, átfordul
        System.out.println(b);
        float f = 5.666f; //f nélkül doubleben tárolja, f = double, f 32bit d 64bit -> veszteség
        float f2 =(float) 5.666; //castolás nélkül doubleben tárolja, f = double, f 32bit d 64bit -> veszteség
        */
        
        Scanner s = new Scanner (System.in); //standard input
        
        
       /* switch(num){
            case 5: System.out.println("Jeles"); break;
            case 4: System.out.println("Jó"); break;
            case 3: System.out.println("közepes"); break;
            case 2: System.out.println("Elégséges"); break;
            
            default: System.out.println("Elégtelen");
        }*/
        
        if (s.hasNextInt()){
            int num = s.nextInt();
            if (num == 5) {
            System.out.println("Jeles");
        } else if (num == 4) {
            System.out.println("Jó");
        } else if (num == 3) {
            System.out.println("Közepes");
        } else if (num == 2) {
            System.out.println("Elégséges");
        } else {
            System.out.println("Elégtelen");
        }
        }
       
        
        
    }

}
