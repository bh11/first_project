package first;


// @author $Pálovics István
 
public class Second {

    public static void main(String[] args) {
        StringBuffer s = new StringBuffer ("12121122");
        //String s = "12121122";
        int n = 0;
        for (int i = 1; i < s.length(); i++){
            if (s.charAt(i) == '1' ){
                n++;
                continue;               
            }
            s.setCharAt(i, '3');
        }
        System.out.println(n);
        System.out.println(s);
    
    }

}
