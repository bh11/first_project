package osztók;

import java.util.Scanner;


// @author $Pálovics István
 
public class Osztók {

    public static void main(String[] args) {
/*        Scanner s = new Scanner(System.in);
        System.out.println("Kérem a számot: ");
        int a = 0;
        if (s.hasNext()){
            a = s.nextInt();
        }
        for (int i = 1; i <= a; i++){
            if (a % i == 0){
                System.out.println(i + " osztója " + a + "nak/nek");
            }
        }
 */   

    Scanner scanner = new Scanner(System.in);
    boolean isRequiredNumber = false;
    int number = -1;
    
    do {
        System.out.println("Adjon meg egy pozitív egész számot!");
        if (scanner.hasNextInt()) {
            number = scanner.nextInt();
            
            if (number > 0) {
                isRequiredNumber = true;
            }
            
        } else {
            scanner.nextLine();
        }
        
    } while(!isRequiredNumber);
    
    for (int i = 1; i <= number; i++){
            if (number % i == 0){
                System.out.println(i + " osztója " + number + "nak/nek");
            }
        }


    }

}
