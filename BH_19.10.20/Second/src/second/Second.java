package second;

import java.util.Scanner;

// @author $Pálovics István
public class Second {

    public static void main(String[] args) {
        /*       byte b = 0;
    while (true) {
        System.out.println(b++);
        Thread.sleep(500);
    }
         */

        Scanner s = new Scanner(System.in);

        int i = 0;
        int sum = 0;
        /*   while (i < 5){
            System.out.println("Kérem a(z)" + (i+1) + ". számot: ");
            sum += s.nextInt();
            i++;
    }
        System.out.println(sum);
         */

        do {
            System.out.println("Legalább egyszer lefut");
        } while (false);

        int stop_number = 0;
        do {
            System.out.println("Kérek egy számot");
            i = s.nextInt();
            if (i != stop_number) {
                System.out.println(i);
            }
        } while (i != stop_number);

    }
}
